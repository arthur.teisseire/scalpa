%{
/* fichier dans lequel est defini la macro constante NOMBRE */

#include "yacc.tab.h"

%}

%option noyywrap

%%
[0-9]+       {return NOMBRE;}
\n|.         {}
%%
