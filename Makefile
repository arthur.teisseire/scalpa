NAME    =    scalpa

CC      =    gcc
CFLAGS  =    -Wall -W -Wextra -I$(INC) -g

INC     =    src

DSRC    =    src
SRC     =    $(DSRC)/lex.yy.c \
             $(DSRC)/yacc.tab.c

MAIN    =    $(DSRC)/main.c

OBJ     =    $(SRC:.c=.o) $(MAIN:.c=.o)


all: pre $(NAME)

pre:
	bison -d $(DSRC)/yacc.y
	mv yacc.tab.c yacc.tab.h $(DSRC)
	lex -ts $(DSRC)/lex.lex > $(DSRC)/lex.yy.c

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(CFLAGS)

clean:
	rm -f $(OBJ) $(DSRC)/lex.yy.c $(DSRC)/yacc.tab.c $(DSRC)/yacc.tab.h

fclean: clean
	rm -f $(NAME)

re: fclean all
